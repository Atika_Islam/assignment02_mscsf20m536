#include <sys/types.h>
#include <dirent.h>
#include<string.h>
#include <stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include <ctype.h>
// Helper function to check if a struct dirent from /proc is a PID folder.
int is_pid_folder(const struct dirent *entry) {
    const char *p;

    for (p = entry->d_name; *p; p++) {
        if (!isdigit(*p))
            return 0;
    }

    return 1;
}
/////////////////////////////////////////////////////////
long getdigits(char *str){
long n = 0;
char * pch= NULL;
pch = strtok (str," ");
int i = 0;
while (pch != NULL)
{
   pch = strtok (NULL, " ");
   i++;
   if(i == 1){
    n = atoi(pch);
    break;
   }
}
return n;
}
//////////////////////////////////////////////////////
float get_KB()
{
	int mem_total=0;
	FILE *fp; // declaration of file pointer
   
  	  fp =fopen("/proc/meminfo","r");// opening of file
	    if (!fp)// checking for error
	    return 1;

		char line[100];
		while(fgets(line,100,fp)){
		
		         if(strncmp(line, "MemTotal:", 9)==0)
		        {
		               mem_total= getdigits(line);
		        }
	  	 	


		}
		//int res1 = res();
		//printf("%d \n", mem_total/1024);
	 fclose(fp); // closing file

	return mem_total;
}
//////////////////////////////////////////////////////

int res ()
{
    DIR *procdir;
    FILE *fp1;
    struct dirent *entry;
    char path[256 + 5 + 5]; // d_name + /proc + /stat
    int pid;
    
     //char COMMAND[100];
    int res=0;
     int stack=0;
      printf("=================================================================================================================\n");
        printf("%s","\t\t\t\tMEM%\t");
        printf("\n=============================================================================================================\n");
   
    procdir = opendir("/proc");
    if (!procdir) {
        perror("opendir failed");
          return 1;
    }
     int mem_kb=0;
	 mem_kb= get_KB();
         
     while ( (entry = readdir(procdir))) {
	
        // Skip anything that is not a PID folder.
        if (!is_pid_folder(entry))
            continue;

        // Try to open /proc/<PID>/stat.
        snprintf(path, sizeof(path), "/proc/%s/statm", entry->d_name);
        fp1 = fopen(path, "r");
	
        if (!fp1) {
            perror(path);
            continue;
        }
	fscanf(fp1, "%d %d %*d %*d %*d %d %*d" ,&pid, & res,& stack);
         int res_stack = (res+ stack)*100;   
         
         
	 float mem_p = (res_stack/mem_kb);
         
	 printf("%12.1f \t",mem_p);
	fclose(fp1);
	}
       
                 
	
	closedir(procdir);
	
}


//////////////////////////////////////////////////////
float uptime()
{
    DIR *procdir;

    struct dirent *entry;
    //char path[256 + 5 + 5]; // d_name + /proc + /stat
    int pid;
    //char status;
    float uptime;
     
    FILE *fp; // declaration of file pointer

    fp =fopen("/proc/uptime","r");// opening of file
    if (!fp)// checking for error
    return 1;
         fscanf(fp, "%f ", & uptime);
        // printf("%f", uptime);      
           
    fclose(fp);
     return uptime;
}

//////////////////////////////////////////////////
int get_command()
{

DIR *procdir;
    FILE *fp;
    struct dirent *entry;
    char path[256 + 5 + 5]; // d_name + /proc + /stat
    int pid;
    char command[1000];
char name[1000];
   int res=0;
    int rssanon=0;
  int shr=0;
 
    // Open /proc directory.
    procdir = opendir("/proc");
    if (!procdir) {
        perror("opendir failed");
        return 1;
    }
        printf("==========================================================================\n");
        printf("%20s %15s %15s","COMMAND" ,"RES" ,"SHR\n");
        printf("==========================================================================\n");
     while ((entry = readdir(procdir))) {
        // Skip anything that is not a PID folder.
        if (!is_pid_folder(entry))
            continue;

        // Try to open /proc/<PID>/stat.
        snprintf(path, sizeof(path), "/proc/%s/status", entry->d_name);
        fp = fopen(path, "r");

        if (!fp) {
            perror(path);
            return -1;
        }


        // Get PID, process name and number of faults.
        fscanf(fp,"%s %s ",&name, &command);
       int  res =0, shr=0;
         char line [100];      
          while(fgets(line,100,fp)){

         
                  if(strncmp(line, "VmRSS:", 6)==0){
                       res= getdigits(line);                        
             }
                 
                  if(strncmp(line, "RssAnon:", 8)==0){
                       rssanon= getdigits(line);                        
             }
}
              shr= res- rssanon;  
           


        printf("%20s %15d %15d\n",command, res,shr);
        fclose(fp);
    }

   closedir(procdir);
    return 0;
}



int main(void) {
    DIR *procdir;
    FILE *fp;
    struct dirent *entry;
    char path[256 + 5 + 5]; // d_name + /proc + /stat
    int pid;
 char name[1000];
    char command[1000];
    char status;
    int pr;
    int ni;
    int frteen;
    int fiftn;
    int virt=0;
    int total=0;
    printf("==================================================================================================================\n");
    printf("%10s %10s %10s %15s %5s %12s %12s","PID", "PR","NI", "VIRT" ,"S", "CPU%" , "Time+");
     
    printf("\n================================================================================================================\n");
    int utime, stime, cutime, cstime, starttime;

    float up = uptime();
  
    int HERTZ= sysconf(_SC_CLK_TCK);

    // Open /proc directory.
    procdir = opendir("/proc");
    if (!procdir) {
        perror("opendir failed");
        return 1;
    }    
     while ((entry = readdir(procdir)))
     {
        // Skip anything that is not a PID folder.
        if (!is_pid_folder(entry))
            continue;

        // Try to open /proc/<PID>/stat.
        snprintf(path, sizeof(path), "/proc/%s/stat", entry->d_name);
        fp = fopen(path, "r");
        //printf("%s \n ", path);
        if (!fp) {
            perror(path);
            continue;
        }
 
        // Get PID, process name and number of faults.
            fscanf(fp, "%d %s %c %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %d %d %d %d %d %d %*lf %*lf %d %d ",&pid , &path, & status,&utime,&stime,&cutime,&cstime,&pr, &ni,&starttime, &virt);
       
       
       
            total = utime+stime/100;
     
           int days_in =0, hours_in =0, mins_in =0;

           hours_in = total / 3600;
           mins_in = (total - (hours_in *3600))/60;
           int  sec=total - (hours_in*3600)-(mins_in*60);


 
              float totaltime= utime+stime;
              totaltime= totaltime+cutime+cstime;
              float seconds = up-(starttime/ HERTZ);

              float  cpu= (totaltime/HERTZ);
              cpu = (float)(cpu/seconds);
              cpu=(float)(cpu*100);
//********************************************************  
             printf(" %10d %10d %10d %15d %5c %12.1f %10d:%d:%d\n",pid,pr,ni,virt,status,cpu,hours_in,mins_in,sec);

              fclose(fp);
    }
 
   closedir(procdir);
   
   get_command();      
   res();
    return 0;
}

