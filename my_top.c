#include <sys/sysinfo.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include<sys/utsname.h>
#include<utmp.h>
#include<time.h>
#include<string.h>
#include<sys/types.h>
#include<dirent.h>
#include<ctype.h>


///%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%GLoal variables%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
int COUNT_PROCESS=0,COUNT_T=0,COUNT_S=0,COUNT_Z=0,COUNT_R=0;



char* get_uptime()
{
    struct sysinfo sys_info;
    int res = sysinfo(&sys_info);
    if(res != 0)
        perror("sysinfo");

	    
     int days_in =0, hours_in =0, mins_in =0;
     days_in = sys_info.uptime / 86400;
     hours_in = (sys_info.uptime / 3600) - (days_in * 24);
     mins_in = (sys_info.uptime / 60) - (days_in * 1440) - (hours_in * 60);
   
     char days[20], hours[20], mins[20];
     sprintf(days, "%d", days_in);
     sprintf(hours,"%d", hours_in);
     sprintf(mins,"%d", mins_in);
     static char uptime[20]="";
      //char days[20], hours[20], mins[20];
     if(days_in ==0)
     {
        if(hours_in!=0 && mins_in!=0)
        {
            strcat(uptime,hours); strcat(uptime,":");
            strcat(uptime,mins);strcat(uptime,"");
             
            // uptime=hours+" hours "+mins+" min ";

        }
        else if(hours_in==0 && mins_in!=0)
        {
            strcat(uptime,mins);strcat(uptime," min ");
           //  uptime=mins+ " min ";
       
        }else if(hours_in!=0 && mins_in==0)
        {
            strcat(uptime,hours);strcat(uptime," hours ");
             //   uptime = hours + " hours ";
       
        }else
        {
            strcat(uptime,mins);strcat(uptime," min");
        }
     }
      else if(days_in!=0)
     {
        strcat(uptime, days); strcat(uptime, " days ");
        if(hours_in!=0 && mins_in==0){    // uptime=days+ " days " +hours+ " hours "; }
            strcat(uptime, hours); strcat(uptime, " hour ");}

       else if(hours_in==0 && mins_in!=0){  //uptime = days+" days "+ mins +" min ";
             strcat(uptime,mins); strcat(uptime, " min ");
        }
        else if(hours_in!=0 && mins_in!=0){
        strcat(uptime, hours); strcat(uptime, ":");
             strcat(uptime, mins); strcat(uptime, "");
        }
     }
     return uptime;
}

int get_user_count()
{
  struct utmp *n;
  setutent();
  n=getutent();
  int count =0;

  while(n) {
   // printf("%d---- %d\n", USER_PROCESS,n->ut_type);
    if(n->ut_type==USER_PROCESS) {
	count++;
     // printf("%9s%12s (%s)\n", n->ut_user, n->ut_line, n->ut_host);
    }
    n=getutent();
  }
 return count;
}

// load avg

int read_ints (float*arr,const char* file_name)
{
  FILE *myfile;
  int i=0;
  float var;

  myfile=fopen(file_name, "r");
  if(myfile==NULL)
return 0;
  for (int j = 0 ; j < 3; j++)
    {
      fscanf(myfile,"%f",&var);
      //printf("%.15f ",myvariable);
      arr[i]=var;
      //printf("%.2f %.2f",var, arr[i]);
      i++;
    }
 


  fclose(myfile);
   return 1;
}

// line 2 tasks
//**************************

int is_pid_folder(const struct dirent *entry) {
    const char *p;

    for (p = entry->d_name; *p; p++) {
        if (!isdigit(*p))
            return 0;
    }

    return 1;
}


//*****************************


void print_line_1()
{
   // struct sysinfo sys_info;
    time_t rawtime;
    struct tm * timeinfo;
   
    // get time
    time( &rawtime );
    timeinfo = localtime( &rawtime );
   
    //get uptime
     char* c = (get_uptime());

     printf("top - %02d:%02d:%02d up %s, %d user, load average: ", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,c, get_user_count() );

    // get loadavg
	
     float a[3] = {0};
     read_ints(a,"/proc/loadavg");
     for(int j=0;j<3;j++)
     printf("%.2f  ",a[j]);
}

//*******************************************************
//int COUNT_P=0,COUNT_S=0,COUNT_Z=0,COUNT_R=0;
void print_line_2()
{
     DIR *procdir;
    FILE *fp;
    struct dirent *entry;
    char path[256 + 5 + 5]; // d_name + /proc + /stat
    int pid;
    char status;
   
    // Open /proc directory.
    procdir = opendir("/proc");
    if (!procdir) {
        perror("opendir failed");
    }
   
    // Iterate through all files and folders of /proc.
    while ((entry = readdir(procdir))) {
        // Skip anything that is not a PID folder.
        if (!is_pid_folder(entry))
            continue;

        // Try to open /proc/<PID>/stat.
        snprintf(path, sizeof(path), "/proc/%s/stat", entry->d_name);
        fp = fopen(path, "r");

        if (!fp) {
            perror(path);
            continue;
        }
  // Get PID, process name and number of faults.
        fscanf(fp, "%d %s %c",
            &pid, &path, &status
        );
        if(status=='R')
                COUNT_R++;
        else if(status=='S')
                COUNT_S++;
        else if(status=='Z')
                COUNT_Z++;
        else if(status=='T')
                COUNT_T++;
        // Pretty print.
       // printf("%5d %-20s: %lu \t %c\n", pid, path, maj_faults,status);
        fclose(fp);
        COUNT_PROCESS++;
    }

    closedir(procdir);
    printf("\nTasks: total %d running %d sleeping %d stopped %d zombie %d \n", COUNT_PROCESS, COUNT_R, COUNT_S,COUNT_Z, COUNT_T );
   
}
/// print line 3 ***********************************************

void print_line_3()
{
        FILE *fp;
        double cpu_us=0, user_t=0,cpu_ni=0, cpu_sy=0, cpu_id=0, io_time=0, hi=0,si=0, steal_t=0;
        double total = 0;

        fp = fopen("/proc/stat", "r");

        if(fp != NULL) {
                fscanf(fp, "%*s %lf %lf %lf %lf %lf %lf %lf %lf %lf",&cpu_us,&user_t, &cpu_ni,
                        &cpu_sy, &cpu_id, &io_time, &hi, &si, &steal_t);
}
        total = cpu_us + cpu_ni + cpu_sy + cpu_id + io_time + hi + si + steal_t;
//        printf(" %lf \n",total);
        double user = cpu_us / total *100;
        double nice = cpu_ni / total *100;
        double system = cpu_sy / total *100;
        double idle = cpu_id / total *100;
        double cpu_wait = io_time / total *100;
        double hardware = hi / total *100;
        double software= si / total *100;
        double steal_time = steal_t / total *100;


        printf("Cpu(s): %0.1f us,  %0.1f sy,  %0.1f ni,  %0.1f id,  %0.1f wa,  %0.1f hi,  %0.1f si, %0.1f st \n", user, system, nice, idle, cpu_wait, hardware, software, steal_time);
        fclose(fp);
}
long getdigits(char *str){
	long n = 0;
	char * pch;
	pch = strtok (str," ");
	// p = pch[0];
	int i = 0;
	while (pch != NULL)
	{
	    pch = strtok (NULL, " ");
	    i++;
	    if(i == 1){
	    	n = atoi(pch);
	    	break;
	    }
	}
	return n;	 
}
void print_line_4()
{
	  int count =0,i=0;
    FILE *fp= NULL; // declaration of file pointer
   // char con[1000]; // variable to read the content
    fp =fopen("/proc/meminfo","r");// opening of file
    if (!fp)// checking for error
	printf("Error opening file");

	int mem_total=0, mem_free=0, cache=0,mem_used=0;
        int mem_available=0, swapt=0, swapfree=0,swp_used=0;
    
	char line[100];
        while(fgets(line,100,fp)){
		
  	     if(strncmp(line, "MemTotal:", 9)==0){
                       mem_total = getdigits(line);
             }			 
              if(strncmp(line, "MemFree:", 8)==0){
                       mem_free = getdigits(line);
             }			
	   	
             if(strncmp(line, "Cached:", 7)==0){
                       cache = getdigits(line);
            }
             
           if(strncmp(line, "MemAvailable:", 13)==0)
                {
                       mem_available = getdigits(line);
                }
               if(strncmp(line, "SwapTotal:", 10)==0)
		{
			swapt= getdigits(line);
                }
                if(strncmp(line, "SwapFree:", 9)==0)
		{
			swapfree= getdigits(line);
                }

         }

        mem_used= mem_total-mem_free-cache;
        swp_used= swapt-swapfree;
  
        printf("KibMem: %d: total\t %d: Free\t %d: Used\t %d: Buffer/Cached\n", mem_total,mem_free, mem_used, cache);
        printf("SwapMem: %d: total\t %d: Free\t %d: Used\t %d: Memavailable \n", swapt,swapfree,swp_used, mem_available);
 
 fclose(fp); // closing file
}

int main()
{
    print_line_1();
    print_line_2();
    print_line_3();
    print_line_4();
    return 0;
}

