#include <stdlib.h>
#include <pwd.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
// Helper function to check if a struct dirent from /proc is a PID folder.
int is_pid_folder(const struct dirent *entry) {
    const char *p;

    for (p = entry->d_name; *p; p++) {
        if (!isdigit(*p))
            return 0;
    }

    return 1;
}
long getdigits(char *str){
	long n = 0;
	char * pch= NULL;
	pch = strtok (str," ");
	int i = 0;
	while (pch != NULL)
	{
	    pch = strtok (NULL, " ");
	    i++;
	    if(i == 1){
	    	n = atoi(pch);
	    	break;
	    }
	}
	return n;	 
}


int main(int argc, char *argv[])
{
    DIR *procdir;
    FILE *fp;
    
 //  int uid=0;
    struct dirent *entry;
    char path[256 + 5 + 5]; // d_name + /proc + /stat
    int pid;
  register struct passwd *pw;
    uid_t  uid;
  int c;
 procdir = opendir("/proc");
    if (!procdir) {
        perror("opendir failed");
        return 1;
    }
        printf("==========================================================================\n");
        printf("USER\t\t\n");
        printf("==========================================================================\n");
     while ((entry = readdir(procdir))) {
        // Skip anything that is not a PID folder.
        if (!is_pid_folder(entry))
            continue;

        // Try to open /proc/<PID>/stat.
        snprintf(path, sizeof(path), "/proc/%s/status", entry->d_name);
        fp = fopen(path, "r");

        if (!fp) {
            perror(path);
            continue;
        }
	 char line[100];
         while(fgets(line,100,fp)){
//		 printf("%//s \t", line);
         
                  if(strncmp(line, "Uid:", 4)==0)
		  	uid= getdigits(line);
			//printf("%u",uid);                        
             

		  	 pw = getpwuid (uid);
		  	if (pw)
		   	 {
			//	puts(pw->pw_uid);
		      		puts (pw->pw_name);
		      		exit (EXIT_SUCCESS);
		    	}
			
		  fprintf (stderr,"%s: cannot find username for UID %u\n",  (unsigned) uid);
		}
}
	
   closedir(procdir);
    return 0;

}

